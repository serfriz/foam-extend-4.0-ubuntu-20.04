export WM_THIRD_PARTY_USE_BISON_27=1
export QT_SELECT=qt4
export WM_CC='gcc-5'
export WM_CXX='g++-5'
export QT_BIN_DIR=/usr/bin/

export WM_MPLIB=SYSTEMOPENMPI
export OPENMPI_DIR=/usr
export OPENMPI_BIN_DIR=$OPENMPI_DIR/bin

export OPENMPI_LIB_DIR="`$OPENMPI_BIN_DIR/mpicc --showme:libdirs`"
export OPENMPI_INCLUDE_DIR="`$OPENMPI_BIN_DIR/mpicc --showme:incdirs`"
export OPENMPI_COMPILE_FLAGS="`$OPENMPI_BIN_DIR/mpicc --showme:compile`"
export OPENMPI_LINK_FLAGS="`$OPENMPI_BIN_DIR/mpicc --showme:link`"
